package com.sda.back.example.controller;

// TODO: listuj zadania konkretnego uzytkownika - parametr asignee(zalog uzytk), zwraca liste taskow
// TODO: zmiana statusu tasku
// TODO: listuj niewykonane/wykonane taski
// TODO: modyfikuj task
// liste pobrac z serwisu i tutaj tylko wyswietlic; uzyc prntln

import com.sda.back.example.model.Statuses;
import com.sda.back.example.service.Services;
import com.sda.back.example.service.TaskService;
import com.sda.back.example.service.TaskServiceImpl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;

public class TaskController {

    private TaskService service = Services.getInstance().getTaskService();

    public void showListOfMyTasks(long asignee){
        System.out.println("Your tasks list: ");
        List list = service.listOfMyTask(asignee);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

    }

    public void showListOfMyTasksDone(long asignee){
        System.out.println("Your tasks (in status DONE) list: ");
        List list = service.listOfDoneTasks(asignee);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }


    public void showListOfMyTasksInProgress(long asignee){
        List list = service.listOfInProgressTasks(asignee);
        for(int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
    }

    public void addTaskOnline(long asignee){

        // ToDO: wyrzucic to do parsera; kontroller przyjmuje TYLKO parametry
        // long asignee, long idTaskOwner, String taskName, Statuses taskStatus, String notes, LocalDateTime deadline
        System.out.println("Dodawanie zadania - argumenty: ");
        Scanner argum = new Scanner(System.in);
        System.out.println("Podaj id wlasciela zadania(long): ");
        long argum1 = argum.nextLong();
        System.out.println("Podaj nazwe zadania: ");
        String argum2 =argum.next();
        System.out.println("Podaj status:");
        String status =argum.next();
        Statuses statuses = Statuses.valueOf(status.trim().toUpperCase());
        System.out.println("Dodatkowe notatki ");
        String argum4 =argum.next();
        System.out.println("Wpisz: task list - do wyswietlenia zadan lub task addTask do kolejego dodania zadania. ");
        service.addTask(asignee,argum1, argum2, statuses, argum4, LocalDateTime.now());
        //System.out.println("Czy chcesz cos zmienic zadaniu?");;

    }

}
