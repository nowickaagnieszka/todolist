package com.sda.back.example.controller;

import com.sda.back.example.service.Services;
import com.sda.back.example.service.UserService;
import com.sda.back.example.service.UserServiceImpl;

import java.util.Optional;

public class UserController {
    private UserService userService =  Services.getInstance().getUserService();

    public void registerUser(String login, String password){

        if(userService.registerUser(login, password)){
            System.out.println("Udalo sie zarejstrowac, Witamy!");
            System.out.println("Aby sie zalogowac wpisz user login i podaj login i haslo");
        }else {
            System.out.println("Nie udalo sie zarejstrowac :/");
        }
    }
    public Optional<Long> login(String login, String pass) {

        return Optional.ofNullable(userService.login(login, pass));
    }
    // W 2 Kontrolerze
    // TODO: do kontrolera metody z serwisu tasku
}
