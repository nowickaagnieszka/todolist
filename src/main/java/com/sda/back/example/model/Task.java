package com.sda.back.example.model;

import java.time.LocalDateTime;

public class Task {
    public long getTaskID() {
        return taskID;
    }

    public void setTaskID(long taskID) {
        this.taskID = taskID;
    }

    private long taskID;

    public long getIdTaskOwner() {
        return idTaskOwner;
    }

    public void setIdTaskOwner(long idTaskOwner) {
        this.idTaskOwner = idTaskOwner;
    }

    private long idTaskOwner;
    private long asignee;
    private String nameOfTask;
    private Statuses taskStatus;
    private String note;
    private LocalDateTime deadline;

    public long getAsignee() {
        return asignee;
    }

    public void setAsignee(long asignee) {
        this.asignee = asignee;
    }

    public Task() {
    }

    public Task(long taskID, long idTaskOwner, long asignee, String nameOfTask, Statuses taskStatus, String note, LocalDateTime deadline) {
        this.taskID = taskID;
        this.idTaskOwner = idTaskOwner;
        this.asignee = asignee;
        this.nameOfTask = nameOfTask;
        this.taskStatus = taskStatus;
        this.note = note;
        this.deadline = deadline;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public Statuses getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Statuses taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskID=" + taskID +
                ", idTaskOwner=" + idTaskOwner +
                ", asignee=" + asignee +
                ", nameOfTask='" + nameOfTask + '\'' +
                ", taskStatus=" + taskStatus +
                ", note='" + note + '\'' +
                ", deadline=" + deadline +
                '}';
    }
}
