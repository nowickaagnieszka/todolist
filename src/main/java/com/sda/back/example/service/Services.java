package com.sda.back.example.service;

public class Services {
    private static final Services INSTANCE = new Services();
    private final UserService userService = new UserServiceImpl();
    private final TaskService taskService = new TaskServiceImpl();

    private Services() {
    }

    public static Services getInstance() {
        return INSTANCE;
    }

    public UserService getUserService() {
        return userService;
    }

    public TaskService getTaskService() {
        return taskService;
    }
}
