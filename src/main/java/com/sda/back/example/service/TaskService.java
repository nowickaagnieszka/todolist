package com.sda.back.example.service;

import com.sda.back.example.model.Statuses;
import com.sda.back.example.model.Task;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

public interface TaskService {

    boolean addTask(long asignee, long idTaskOwner, String taskName, Statuses taskStatus, String notes, LocalDateTime deadline);

    List<Task> listOfDoneTasks(long asignee);
    List<Task> listOfPendingTasks(long asignee);
    List<Task> listOfInProgressTasks(long asignee);
    List<Task> listOfMyTask(long asignee);
    void changeStatus(long idTask, Statuses status);


    // TODO: listuj zadania konkretnego uzytkownika - parametr asignee, zwraca liste taskow
    // TODO: zmiana statusu tasku
    // TODO: listuj niewykonane/wykonane taski
    // TODO: modyfikuj task
    // TODO: https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/

}
