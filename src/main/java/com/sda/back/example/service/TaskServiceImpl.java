package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.Statuses;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TaskServiceImpl implements TaskService {

    private static int TASK_COUNTER = 0;

    public HashMap<Long, Task> taskLists = new HashMap<>();

    // TODO:
    public TaskServiceImpl() {
        // this.taskLists = // załadować dane z pliku.
        // TODO: zeby zaladowac taski z pliku trzeba stworzyc klase ktora sie tym zajmuje
        // TODO: ta sama klasa musi miec mozliwosc zapisywania tych taskow z mapy ^^^
        // TODO: sugerowany format:
        // entry=>
        // linia: id;idTaskOwner;asignee;nameOfTask;taskStatus;note;deadline
        // TODO: split po średnikach zeby oddzielic elementy taska
    }

    @Override
    public boolean addTask(long asignee, long idTaskOwner, String taskName, Statuses taskStatus, String notes, LocalDateTime deadline) {
        // TODO: po dodaniu taska zapisujemy plik
        if (idTaskOwner <= 0) {
            return false;
        }

        Task newTask = new Task(TASK_COUNTER++, idTaskOwner, asignee, taskName, taskStatus, notes, deadline);
        taskLists.put(newTask.getTaskID(), newTask);
        return false;
    }

    @Override
    public List<Task> listOfDoneTasks(long asignee) {
        List<Task> list = taskLists.values()
                .stream()
                .filter(task -> task.getAsignee() == asignee && task.getTaskStatus() == Statuses.DONE)
                .collect(Collectors.toList());
        return list;
    }

    @Override
    public List<Task> listOfPendingTasks(long asignee) {
        List<Task> list = taskLists.values()
                .stream()
                .filter(task -> task.getAsignee() == asignee && task.getTaskStatus() == Statuses.DO)
                .collect(Collectors.toList());
        return list;
    }

    @Override
    public List<Task> listOfInProgressTasks(long asignee) {
        List<Task> list = taskLists.values()
                .stream()
                .filter(task -> task.getAsignee() == asignee && task.getTaskStatus() == Statuses.IN_PROGRESS)
                .collect(Collectors.toList());
        return list;
    }

    @Override
    public List<Task> listOfMyTask(long asignee) {
        List<Task> list = taskLists.values()
                .stream()
                .filter(task -> task.getAsignee() == asignee)
                .collect(Collectors.toList());
        return list;
    }

    @Override
    public void changeStatus(long idTask, Statuses status) {
        // TODO: po change status zapisujemy caly plik
        Task tempValue = taskLists.get(idTask);
        tempValue.setTaskStatus(status);

        taskLists.put(idTask, tempValue);

    }


}
