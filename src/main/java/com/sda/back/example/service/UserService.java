package com.sda.back.example.service;

import com.sda.back.example.model.User;

public interface UserService {
    public User getUserWithId(long id);
    boolean userExists(String login);
    boolean registerUser(String login, String password);
    Long login(String login, String pass);

    // TODO: logowanie uzytkownika
}
