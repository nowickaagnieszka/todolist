package com.sda.back.example.service;

import com.sda.back.example.model.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {
    private static long USER_ID_COUNTER = 1;
    private Map<Long, User> users = new HashMap<Long, User>();

    protected UserServiceImpl() {
        //TODO: w konstruktorze zaladowac wszystkich userow z pliku do mapy.
    }

    public User getUserWithId(long id) {
        return users.get(id);
    }

    @Override
    public boolean userExists(String login) {
        return false;
    }

    public boolean registerUser(String login, String password) {
        // TODO: po rejestracji dokonac pelnego zapisu całej mapy do pliku
        // jesli uzytk istnieje, to nie mozemy go zajestrowac
        if (userExists(login)) {
            return false;
        }
        String hashed = DigestUtils.md5Hex(password).toLowerCase();

        System.out.println("Encrypted password is: " + hashed);

        User newUser = new User(USER_ID_COUNTER++, login, hashed);

        users.put(newUser.getId(), newUser);
        return true;
    }

    @Override
    public Long login(String login, String pass) {
        List<User> possibleUsers = users.values()
                .stream()
                .filter(user -> user.getLogin().equals(login))
                .collect(Collectors.toList());

        if (possibleUsers.size() == 1) {
            String hashed = DigestUtils.md5Hex(pass).toLowerCase();
            User u = possibleUsers.get(0);
            if (u.getPasswordHash().equals(hashed)) {
                return u.getId();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}

