package com.sda.back.example.views;

import com.sda.back.example.controller.TaskController;
import com.sda.back.example.controller.UserController;

import java.util.Optional;

public class CommandParser {
    private UserController userController = new UserController();
    private SessionCookies cookies = new SessionCookies();
    private TaskController taskController = new TaskController();

    public void parseLine(String line){

        String[] words = line.trim().split(" ");
        if (words[0].toLowerCase().equals("user")){
            parseUserCommand(words);
        }else if(words[0].equals("task")){
            parseTaskCommand(words);
        }
    }

    private void parseTaskCommand(String[] words) {
        if(cookies.isLoggedIn()) {
            if (words[1].equals("list")) {
                taskController.showListOfMyTasks(cookies.getLoggedUserId());
            }
            if (words[1].equals("addTask")) {
                taskController.addTaskOnline(cookies.getLoggedUserId());
            }
        }
    }

    private void parseUserCommand(String[] words) {
        if (words[1].equals("register")){
            userController.registerUser(words[2],words[3]);
        }else if(words[1].equalsIgnoreCase("login")){
            // jeśli nie jesteśmy zalogowani
            if (!cookies.isLoggedIn()) {
                // próbujemy sie zalogować
                Optional<Long> userId = userController.login(words[2], words[3]);
                if (userId.isPresent()) {
                    // zalogowani
                    System.out.println("Zalogowany");
                    System.out.println("Wpisz task addTask");
                    cookies.setLoggedIn(true);
                    cookies.setLoggedUserId(userId.get());
                } else {
                    // nieudana próba logowania
                    System.out.println("NZalogowany");
                    cookies.setLoggedIn(false);
                    cookies.setLoggedUserId(null);
                }
            }else{
                System.out.println("Jestes juz zalogoawny. Nie mozna drugi raz.");
            }
        }
    }
}
