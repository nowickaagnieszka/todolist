package com.sda.back.example.views;

import com.sda.back.example.model.Statuses;
import com.sda.back.example.service.TaskServiceImpl;

import java.time.LocalDateTime;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        CommandParser parser = new CommandParser();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj: user register login pw");

        while (sc.hasNext()){
            String line = sc.nextLine();

            if (line.equalsIgnoreCase("quit")){
                break;
            }else {
                parser.parseLine(line);
            }
        }

    }

}
