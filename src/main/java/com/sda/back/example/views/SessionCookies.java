package com.sda.back.example.views;

import java.util.Optional;

public class SessionCookies {
    private boolean isLoggedIn = false;
    private String sessionId = null;
    private Long loggedUserId;

    public SessionCookies() {
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setLoggedUserId(Long loggedUserId) {
        this.loggedUserId = loggedUserId;
    }

    public Long getLoggedUserId() {
        return loggedUserId;
    }
}
